namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.User", "Name", c => c.String());
            AddColumn("dbo.User", "Email", c => c.String());
            AddColumn("dbo.User", "BirthDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.User", "TradeExperience", c => c.String());
            AddColumn("dbo.User", "Title", c => c.String());
            AddColumn("dbo.User", "PhoneNumber", c => c.String());
            DropPrimaryKey("dbo.User", new[] { "UserId" });
            AddPrimaryKey("dbo.User", "Id");
            DropColumn("dbo.User", "UserId");
            DropColumn("dbo.User", "UserName");
            DropColumn("dbo.User", "temp");
        }
        
        public override void Down()
        {
            AddColumn("dbo.User", "UserName", c => c.String());
            AddColumn("dbo.User", "UserId", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.User", new[] { "Id" });
            AddPrimaryKey("dbo.User", "UserId");
            DropColumn("dbo.User", "PhoneNumber");
            DropColumn("dbo.User", "Title");
            DropColumn("dbo.User", "TradeExperience");
            DropColumn("dbo.User", "BirthDate");
            DropColumn("dbo.User", "Email");
            DropColumn("dbo.User", "Name");
            DropColumn("dbo.User", "Id");
        }
    }
}
